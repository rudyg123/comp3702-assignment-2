clc;
%% Line 1 -> 3 variables
level = 5;
discountFactor = 0.95;
slipPenalty = round(rand()*3 + 1);
breakdownPenalty = round(rand()*3 + 1);
N = 30; % number of cells
MaxT = 80; % maximum number of steps

%% configurations
possible_terrains = [{'dirt-straight-hilly'},{'dirt-straight-flat'},{'dirt-slalom-hilly'},{'dirt-slalom-flat'}...
    ,{'asphalt-straight-hilly'},{'asphalt-straight-flat'},{'asphalt-slalom-hilly'},{'asphalt-slalom-flat'}];

possible_cars = [{'mazda'},{'toyota'},{'ferarri'},{'humvee'},{'go-kart'}];

possible_drivers = [{'stig'},{'schumacher'},{'anakin'},{'mushroom'},{'crash'}];

possible_tires = [{'all-terrain'},{'mud'},{'low-profile'},{'performance'}];

%% Line 1 - 3 file writting
fprintf('%s\n',num2str(level));
fprintf('%s %s %s\n',num2str(discountFactor),num2str(slipPenalty),num2str(breakdownPenalty));
fprintf('%s %s\n',num2str(N),num2str(MaxT));

%% Create Track map
terrain_list = [];
for j = 1:N
    index = round(rand()*(length(possible_terrains)))+1;
    if index > length(possible_terrains)
        index = index -1;
    end
    terrain_list = [terrain_list, (possible_terrains(index))];
end

%% Terrain
for j = 1:length(possible_terrains)
    terrain = possible_terrains(j);
    terrain = terrain{1};

    fprintf('%s:',terrain);
    
    current_list = [];
    
    % sort track into terrains
    for jj = 1:N
        current_terrain = terrain_list(jj);
        current_terrain = current_terrain{1};
 
        if  length(current_terrain) == length(terrain)
            if current_terrain == terrain
                current_list = [current_list, jj];
            end
        end
    end
    
    % write to output file
    for jj = 1:length(current_list)
        fprintf('%d',current_list(jj));
        if (jj ~= length(current_list))
           fprintf(','); 
        end
    end
    
    fprintf('\n');
end

%% Cars
fprintf('%s\n',num2str(length(possible_cars)));

for j = 1:length(possible_cars)
    car = possible_cars(j);
    car = car{1};
    fprintf('%s:',car);
    
    prob_of_moving_forward = rand()*0.7;
    probability_array = get_weighted_array(prob_of_moving_forward);
    for jj = 1:length(probability_array)
        fprintf(' %s',num2str(probability_array(jj)));
    end
    fprintf('\n');
end

%% Drivers
fprintf('%d\n',length(possible_drivers));

for j = 1:length(possible_drivers)
    driver = possible_drivers(j);
    driver = driver{1};
    fprintf('%s:',driver);
    
    prob_of_moving_forward = rand()*0.7;
    probability_array = get_weighted_array(prob_of_moving_forward);
    for jj = 1:length(probability_array)
        fprintf(' %s',num2str(probability_array(jj)));
    end
    fprintf('\n');
end

%% Tires
for j = 1:length(possible_tires)
    tire = possible_tires(j);
    tire = tire{1};
    fprintf('%s:',tire);
    
    prob_of_moving_forward = rand()*0.7;
    probability_array = get_weighted_array(prob_of_moving_forward);
    for jj = 1:length(probability_array)
        fprintf(' %s',num2str(probability_array(jj)));
    end
    fprintf('\n');
end

%% Fuel usage
for j = 1:(length(possible_terrains)*length(possible_cars))
    usage = abs(round(randn()))+2;
    fprintf('%d ',usage);
end

fprintf('\n');

%% Slip probability
for j = 1:length(possible_terrains)
    probability = abs(randn());
    if probability > 0.5
        probability = probability/5;
    end
    fprintf('%s ',num2str(probability));
end

fprintf('\n');