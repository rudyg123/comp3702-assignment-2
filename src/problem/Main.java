package problem;

import simulator.Simulator;
import simulator.State;
import solution.MCTS;
import solution.Node;
import solution.PerformanceTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ProblemSpec ps;
        String input_file_string = "examples/level_5/input_lvl5_2.txt";
        String output_file_string = "test_output.txt";

        if (args.length == 2) {
            input_file_string = args[0];
            output_file_string = args[1];
        }
        else {System.out.println("Not the correct number of input arguments, running default...\n");}

        try {
            ps = new ProblemSpec(input_file_string);
            System.out.println(ps.toString());
            State initialState = State.getStartState(ps.getFirstCarType(),
                    ps.getFirstDriver(), ps.getFirstTireModel());
            Node initialNode = new Node(null, initialState, 0, null);
            Simulator mainSimulator = new Simulator(ps, output_file_string);
            mainSimulator.setVerbose(true);

            State currentState = initialState;
            Node currentNode = initialNode;

            boolean failure = false; // Set true after maximum number of time steps is set


            //Variable for performance testing:
            PerformanceTest pt = new PerformanceTest();

            while (!mainSimulator.isGoalState(currentState)&&!failure) {

                long startTime = System.currentTimeMillis();

                try {
                    MCTS currMCTS = new MCTS(currentNode, ps);
                    Action nextAction = currMCTS.returnBestAction();

                    State nextState = mainSimulator.step(nextAction);

                    Node nextNode = new Node(currentNode, nextState,
                            currentNode.getStepsFromRoot()+1, nextAction);

                    currentState = nextState.copyState();
                    currentNode = nextNode.copyNode();
                    currentNode.resetParent();

                    System.out.println("This step took " +
                            (System.currentTimeMillis() - startTime)
                            + " milliseconds\n\n");
//                    System.out.println((System.currentTimeMillis() - startTime)+"\t");

                    pt.getNumExpandedNodes(currMCTS.getNumExpandedNodes());
                    pt.getStepNumbers(mainSimulator.getSteps());
                }

                catch (NullPointerException e){
                    System.err.println("Null pointer exception somewhere!");
                    failure = true;
                }

                // Performance Testing:
                pt.step();

            }
            // Memory and Time Testing:
            System.out.print(pt.toString());

        } catch (IOException e) {
            System.out.println("IO Exception occurred");
            System.exit(1);
        }

        System.out.println("Finished loading!");

    }
}
