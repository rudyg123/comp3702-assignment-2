package solution;


import problem.*;
import simulator.Simulator;
import simulator.State;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Class which performs the MCTS algorithm to decide which action has the
 * highest probability (now or in the future) to move the car forward.
 *
 * If the action is not a MOVE action, the following one from the MCTS output
 * should be a MOVE.
 *
 */
public class MCTS {

    private Node currentRoot;
    private State currentState;
    private ProblemSpec ps;
    private List<ActionType> actionTypes;
    private final long timeSeconds = 10;
    private double forwardMoveThreshold = 0.4;
    private double executeMoveThreshold = 0.7;
    private double slipBreakdownThreshold = 0.4;
    private int numExpandedNodes;

    public MCTS (Node root, ProblemSpec problemSpec) {
        this.currentRoot = root;
        this.currentState = root.getState();
        this.ps = problemSpec;
        this.actionTypes = ps.getLevel().getAvailableActions();
    }

    // Function that will run simulations until time has run out and then
    // give the best action to take
    public Action returnBestAction () {

        // If the root of the tree has no children, it is known that that is
        // the starting state
//        long startTime = System.currentTimeMillis()*1000; //Seconds

        Node selectedNode;
//        while((System.currentTimeMillis()*1000 - startTime) <= timeSeconds) {
        selectedNode = selectionPhase(currentRoot);
        expansionPhase(selectedNode);

        if (selectedNode.getChildrenSize() == 0) {
            this.forwardMoveThreshold = 0.2;
            expansionPhase(selectedNode);
        } else if (selectedNode.getChildrenSize() == 1) {
            // If there is only one child and it is a move action,
            // execute that move action (no need to simulate)
            Action soleAction =
                    selectedNode.getChildren().get(0).getParentAction();

            if (soleAction.getActionType() == actionTypes.get(0)) {
                return soleAction;
            }

            // If there is only one possible solution (to fill fuel),
            // execute that move without simulation
            if (ps.getLevel().getLevelNumber() > 1) {
                if (soleAction.getActionType() == actionTypes.get(4)) {
                    return soleAction;
                }
            }
        } else if (selectedNode.getChildrenSize() < 5) {
            this.forwardMoveThreshold = 0.2;
            expansionPhase(selectedNode);
        }

        this.forwardMoveThreshold = 0.4;

        for (Node expandedNode : selectedNode.getChildren()) {
            simulatePhase(expandedNode);
        }
//        }

        Node highestWinNode = getHighestWinsChild(selectedNode.getChildren());
        return highestWinNode.getParentAction();
    }

    private Node getHighestWinsChild(List<Node> children) {
        Node bestNode = new Node();

        for (Node node : children) {
            if (node.getWins() > bestNode.getWins()) {
                bestNode = node;
            }
        }

        if (bestNode.getWins() > 0) {
            return bestNode;
        } else {
            return children.get(0);
        }
    }

    private Node selectionPhase(Node currentRoot) {

        //@TODO ALWAYS PICKS THE ROOT NODE (i.e. no 'selection' is occurring
        // currently)
//        System.out.println(currentRoot.getChildrenSize());

        // Dont want to return a copy. We need to know what the memory
        Node currentNode = currentRoot;
        while(currentNode.getChildrenSize() != 0) { // NEVER GOES IN HERE RN
            Node nextNode = getHighestWeightedChild(currentNode.getChildren());
            currentNode = nextNode;
        }

//        System.out.println("Selected node's state: " + currentNode.getState());
        return currentNode;
    }

    private Node getHighestWeightedChild(List<Node> children) {
        Node bestNode = new Node();

        for (Node node : children) {
            if (node.getWeight() > bestNode.getWeight()) {
                bestNode = node;
            }
        }

        if (bestNode.getWeight() > 0) {
            return bestNode;
        } else {
            return children.get(0);
        }
    }

    private void expansionPhase(Node selectedNode) {

        int fuel;
        String car, driver;
        Tire tire;
        TirePressure pressure;
        OurSim currSim = new OurSim(ps, selectedNode.getState());

        List<ActionType> validActionTypes = ps.getLevel().getAvailableActions();
        List<TirePressure> validPressures = new LinkedList<>();
        validPressures.add(TirePressure.FIFTY_PERCENT);
        validPressures.add(TirePressure.SEVENTY_FIVE_PERCENT);
        validPressures.add(TirePressure.ONE_HUNDRED_PERCENT);

//        System.out.println(validActionTypes);

        // Assume all actions right now only take 1 step to execute
        int actionSteps = 1;

        if (selectedNode.getState().getFuel() < currSim.getFuelConsumption()) {
            int currentFuel = currentState.getFuel();
            int fuelToFill = 59 - currentFuel;
            fuel = (fuelToFill/10) * 10;

            System.out.println("FILLING " + fuel + " LITRES OF FUEL");

            ActionType actionType = actionTypes.get(4); //Fill fuel action type
            Action action = new Action(actionType, fuel);
            currSim = new OurSim(ps, selectedNode.getState());
            State nextState = currSim.step(action);

            selectedNode.updateChildren(new Node(selectedNode, nextState,
                    selectedNode.getStepsFromRoot() + fuel/10,
                    action));
            return;
        }

        if (getForwardMoveProbs(selectedNode.getState()) >= executeMoveThreshold) {
            ActionType actionType = actionTypes.get(0);
            Action action = new Action(actionType);
            currSim = new OurSim(ps, selectedNode.getState());
            State nextState = currSim.step(action);

            selectedNode.updateChildren(new Node(selectedNode, nextState,
                    selectedNode.getStepsFromRoot() + actionSteps,
                    action));
            return;
        }

        for (ActionType actionType : validActionTypes) {
            switch (actionType.getActionNo()) {
                case 1: // MOVE ACTION
                    Action action = new Action(actionType);
                    int numMoveStates;
                    for (numMoveStates = 1; numMoveStates > 0; numMoveStates--) {
                        currSim = new OurSim(ps, selectedNode.getState());
                        State nextState = currSim.step(action);

                        // Ensure that the move action that is expanded is a
                        // forward move
                        while((nextState.getPos() - selectedNode.getState().getPos()) < 1) {
                            currSim = new OurSim(ps, selectedNode.getState());
                            nextState = currSim.step(action);
                        }

                        if (getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                            selectedNode.updateChildren(new Node(selectedNode, nextState,
                                    selectedNode.getStepsFromRoot() + actionSteps,
                                    action));
                        }
                    }
                    break;

                case 2: // CHANGE CAR (ADD ALL THE POSSIBLE CHANGES)
                    for (int i = 0; i < ps.getCarOrder().size(); i++) {
                        String currCar = selectedNode.getState().getCarType();
                        car = ps.getCarOrder().get(i);
                        if (currCar.equals(car)) {
                            continue;
                        }

                        action = new Action(actionType, car);
                        currSim = new OurSim(ps, selectedNode.getState());
                        State nextState = currSim.step(action);

                        if (getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                            selectedNode.updateChildren(new Node(selectedNode, nextState,
                                    selectedNode.getStepsFromRoot() + actionSteps,
                                    action));
                        }
                    }
                    break;
                case 3: // CHANGE DRIVER
                    for (int i = 0; i < ps.getDriverOrder().size(); i++) {
                        String currDriver =
                                selectedNode.getState().getDriver();
                        driver = ps.getDriverOrder().get(i);
                        if (currDriver.equals(driver)) {
                            continue;
                        }

                        action = new Action(actionType, driver);
                        currSim = new OurSim(ps, selectedNode.getState());
                        State nextState = currSim.step(action);

                        if (getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                            selectedNode.updateChildren(new Node(selectedNode, nextState,
                                    selectedNode.getStepsFromRoot() + actionSteps,
                                    action));
                        }
                    }
                    break;
                case 4: // CHANGE TIRE TYPE
                    for (int i = 0; i < ps.getTireOrder().size(); i++) {
                        Tire currTire =
                                selectedNode.getState().getTireModel();
                        tire = ps.getTireOrder().get(i);

                        if (currTire.equals(tire)) {
                            continue;
                        }

                        action = new Action(actionType, tire);
                        currSim = new OurSim(ps, selectedNode.getState());
                        State nextState = currSim.step(action);

                        if (getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                            selectedNode.updateChildren(new Node(selectedNode, nextState,
                                    selectedNode.getStepsFromRoot() + actionSteps,
                                    action));
                        }
                    }
                    break;
                case 5: // DON'T TRY A FUEL ACTION UNTIL REQUIRED
                    break;
                case 6: // ADD ALL VALID PRESSURES
                    for (int i = 0; i < validPressures.size(); i++) {
                        TirePressure currPressure =
                                selectedNode.getState().getTirePressure();
                        pressure = validPressures.get(i);
                        if (pressure.equals(currPressure)) {
                            continue;
                        }

                        action = new Action(actionType, pressure);
                        currSim = new OurSim(ps, selectedNode.getState());
                        State nextState = currSim.step(action);
                        if(getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                            selectedNode.updateChildren(new Node(selectedNode, nextState,
                                    selectedNode.getStepsFromRoot() + actionSteps,
                                    action));
                        }
                    }
                    break;
                case 7: // CHANGE CAR AND DRIVER
                    for (int i = 0; i < ps.getDriverOrder().size(); i++) {
                        String currDriver =
                                selectedNode.getState().getDriver();
                        driver = ps.getDriverOrder().get(i);
                        if (currDriver.equals(driver)) {
                            continue;
                        }

                        for (int j = 0; j < ps.getCarOrder().size(); j++) {
                            String currCar = selectedNode.getState().getCarType();
                            car = ps.getCarOrder().get(j);
                            if (currCar.equals(car)) {
                                continue;
                            }

                            action = new Action(actionType, car, driver);
                            currSim = new OurSim(ps, selectedNode.getState());
                            State nextState = currSim.step(action);

                            if (getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                                selectedNode.updateChildren(new Node(selectedNode, nextState,
                                        selectedNode.getStepsFromRoot() + actionSteps,
                                        action));
                            }
                        }
                    }
                    break;
                default: // CHANGE TIRE FUEL AND PRESSURE
                    for (int i = 0; i < ps.getTireOrder().size(); i++) {
                        Tire currTire =
                                selectedNode.getState().getTireModel();
                        tire = ps.getTireOrder().get(i);
                        if (currTire.equals(tire)) {
                            continue;
                        }

                        for (int j = 0; j < validPressures.size(); j++) {
                            pressure = validPressures.get(j);

                            int currentFuel = currentState.getFuel();
                            int fuelToFill = 59 - currentFuel;
                            fuel = (fuelToFill/10) * 10;

                            action = new Action(actionType, tire, fuel, pressure);
                            currSim = new OurSim(ps, selectedNode.getState());
                            State nextState = currSim.step(action);
                            if(!checkRepeatedAction(action, selectedNode) && getForwardMoveProbs(nextState) > forwardMoveThreshold && getFailProb(nextState) < slipBreakdownThreshold) {
                                selectedNode.updateChildren(new Node(selectedNode, nextState,
                                        selectedNode.getStepsFromRoot() + actionSteps,
                                        action));
                            }
                        }
                    }
            }
        }

        System.out.println("NUMBER OF EXPANDED NODES (ACTIONS) FROM " +
                "SELECTED NODE = " + selectedNode.getChildrenSize());
//        System.out.print(selectedNode.getChildrenSize() +"\t");
        numExpandedNodes = selectedNode.getChildrenSize();
    }

    private void simulatePhase(Node expandedNode) {
        Boolean val;

        int numTries = 10000;

        for (int i=numTries; i!=0; i--) {
            OurSim sim = new OurSim(ps, expandedNode.getState());

            Node tempNode = expandedNode.copyNode();
            while (tempNode.getStepsFromRoot() != ps.getMaxT()) {

                Action nextSimAction = selectRandomAction(ps);
                State nextState = sim.step(nextSimAction);
                val = sim.isGoalState(nextState);

                if (val) {
                    expandedNode.addWin(); // This handles the back-propagation phase
                    break;
                }

                Node nextNode = new Node(tempNode, nextState,
                        tempNode.getStepsFromRoot() + 1, nextSimAction);

                tempNode = nextNode;
            }
            expandedNode.addSim(); // This handles the back-propagation phase
        }
    }

//    private void simulationPhaseMETHOD2(Node expandedNode) {
//        int nextPos;
//
//        int numTries = 10000;
//
//        for (int i = numTries; i != 0; i--) {
//            Simulator sim = new Simulator(ps);
//
//            Node tempNode = expandedNode.copyNode();
//            int currPos = tempNode.getState().getPos();
//
//            while (tempNode.getStepsFromRoot() != ps.getMaxT()) {
//
//                Action nextSimAction = selectRandomAction(ps);
//
////                while(checkRepeatedAction(nextSimAction, )) {
////                    nextSimAction = selectRandomAction(ps);
////                    System.out.println("Next action was the same as previous " +
////                            "action");
////                }
//
//                State nextState = sim.step(nextSimAction);
////                System.out.println(nextState);
//
//                if (nextState == null) {
//                    break;
//                }
//                nextPos = nextState.getPos();
//                int val = nextPos - currPos;
//
//                if (val >= ps.getN() / 5) {
//                    expandedNode.addWin(); // This handles the back-propagation phase
//                    break;
//                }
//
//                Node nextNode = new Node(tempNode, nextState,
//                        tempNode.getStepsFromRoot() + 1, nextSimAction);
//
//                tempNode = nextNode;
//            }
//            expandedNode.addSim(); // This handles the back-propagation phase
//        }
//    }

    /** Select a random action
     *
     * @param ps the problem spec
     * @return a random valid action for problem spec
     */
    private Action selectRandomAction(ProblemSpec ps) {

        int fuel;
        String car, driver;
        Tire tire;
        TirePressure pressure;

        List<ActionType> validActionTypes = ps.getLevel().getAvailableActions();
        List<TirePressure> validPressures = new LinkedList<>();
        validPressures.add(TirePressure.FIFTY_PERCENT);
        validPressures.add(TirePressure.SEVENTY_FIVE_PERCENT);
        validPressures.add(TirePressure.ONE_HUNDRED_PERCENT);

        int numActions = validActionTypes.size();
        int CT = ps.getCT();
        int DT = ps.getDT();
        int TiT = ProblemSpec.NUM_TYRE_MODELS;
        int PressureT = ProblemSpec.TIRE_PRESSURE_LEVELS;

        ActionType actionType = validActionTypes.get(aRandomInt(0, numActions));
        Action action;

        switch(actionType.getActionNo()) {
            case 1:
                action = new Action(actionType);
                break;
            case 2:
                car = ps.getCarOrder().get(aRandomInt(0, CT));
                action = new Action(actionType, car);
                break;
            case 3:
                driver = ps.getDriverOrder().get(aRandomInt(0, DT));
                action = new Action(actionType, driver);
                break;
            case 4:
                tire = ps.getTireOrder().get(aRandomInt(0, TiT));
                action = new Action(actionType, tire);
                break;
            case 5:
                fuel = aRandomInt(ProblemSpec.FUEL_MIN, ProblemSpec.FUEL_MAX);
                action = new Action(actionType, fuel);
                break;
            case 6:
                pressure = validPressures.get(aRandomInt(0, PressureT));
                action = new Action(actionType, pressure);
                break;
            case 7:
                car = ps.getCarOrder().get(aRandomInt(0, CT));
                driver = ps.getDriverOrder().get(aRandomInt(0, DT));
                action = new Action(actionType, car, driver);
                break;
            default:
                tire = ps.getTireOrder().get(aRandomInt(0, TiT));
                fuel = aRandomInt(ProblemSpec.FUEL_MIN, ProblemSpec.FUEL_MAX);
                pressure = validPressures.get(aRandomInt(0, PressureT));
                action = new Action(actionType, tire, fuel, pressure);
        }

        return action;
    }

    /** I'm a helper method */
    private static int aRandomInt(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min)) + min;
    }


    /**
     * Returns true if the next action has been repeated
     *
     * @param nextAct
     * @param currNode
     * @return
     */
    public boolean checkRepeatedAction(Action nextAct, Node currNode) {
        if (currNode.getParentAction() == nextAct) {
            return true;
        }
        return false;
    }


    public double getForwardMoveProbs(State trialState) {
        OurSim tempSim = new OurSim(ps, trialState);
        double[] probs = tempSim.getMoveProbs();

        double sum = 0;
        for (int i = 5; i<= 9; i++) {
            sum += probs[i];
        }

//        System.out.println("FORWARD MOVE PROB OF " + trialState + "= " + sum);
        return sum;
    }

    public double getFailProb(State trialState) {
        OurSim tempSim = new OurSim(ps, trialState);
        double[] probs = tempSim.getMoveProbs();

        return probs[10] + probs[11];
    }

    public int getNumExpandedNodes() {
        return numExpandedNodes;
    }
}
