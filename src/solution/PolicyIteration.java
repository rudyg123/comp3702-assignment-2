//package solution;
//
//import com.sun.org.apache.bcel.internal.generic.RETURN;
//import problem.*;
//import simulator.Simulator;
//import simulator.State;
//
//import java.util.*;
//
//public class PolicyIteration {
//
//    private ProblemSpec ps;
//    private double discountFactor;
//    private final double MAX_REWARD = 3;
//    private List<ActionType> actionTypes;
//    private List<Action> validActions;
//    private Map<State, Action> policyMap = new HashMap<>();
//    List<TirePressure> validPressures = new LinkedList<>();
//
//    public PolicyIteration(ProblemSpec ps) {
//        this.ps = ps;
//        this.discountFactor = ps.getDiscountFactor();
//        this.actionTypes = ps.getLevel().getAvailableActions();
//    }
//
//    public Map<State, Action> solveValueIteration() {
//
//
//        validPressures.add(TirePressure.FIFTY_PERCENT);
//        validPressures.add(TirePressure.SEVENTY_FIVE_PERCENT);
//        validPressures.add(TirePressure.ONE_HUNDRED_PERCENT);
//
//        Map<Action, Integer> policyValue = new HashMap<Action, Integer>();
//        LinkedHashMap<Terrain, List<Integer>> terrainMap = ps.getTerrainMap();
//        for (int pos = 0; pos <= ps.getN(); pos++)  {
//            for (Tire tire : ps.getTireOrder()) {
//                for (String driver : ps.getDriverOrder()) {
//                    for (String car : ps.getCarOrder()) {
//                        for (TirePressure pressure : validPressures) {
//                            for (int fuel = 0; fuel <= 50; fuel++) {
//                                State currState =
//                                        new State(pos, false, false, car, fuel,
//                                                pressure, driver, tire);
//                                generateActionSet(currState);
//
//                                Action bestAction = getBestAction(currState);
//                                policyMap.put(currState, bestAction);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    public void generateActionSet(State currState) {
//
//        validActions.clear();
//
//        int fuel;
//        String car, driver;
//        Tire tire;
//        TirePressure pressure;
//        OurSim currSim = new OurSim(ps, currState);
//
//        // Assume all actions right now only take 1 step to execute
//        int actionSteps = 1;
//
//        if (currState.getFuel() < currSim.getFuelConsumption()) {
//            int currentFuel = currState.getFuel();
//            int fuelToFill = 50 - currentFuel;
//            fuel = (fuelToFill/10) * 10;
//
//            ActionType actionType = actionTypes.get(4); //Fill fuel action type
//            Action action = new Action(actionType, fuel);
//            validActions.add(action);
//            return;
//        }
//
//        for (ActionType actionType : actionTypes) {
//            switch (actionType.getActionNo()) {
//                case 1: // MOVE ACTION
//                    Action action = new Action(actionType);
//                    validActions.add(action);
//                    break;
//                case 2: // CHANGE CAR (ADD ALL THE POSSIBLE CHANGES)
//                    for (int i = 0; i < ps.getCarOrder().size(); i++) {
//                        String currCar = currState.getCarType();
//                        car = ps.getCarOrder().get(i);
//                        if (currCar.equals(car)) {
//                            continue;
//                        }
//
//                        action = new Action(actionType, car);
//                        validActions.add(action);
//                    }
//                    break;
//                case 3: // CHANGE DRIVER
//                    for (int i = 0; i < ps.getDriverOrder().size(); i++) {
//                        String currDriver =
//                                currState.getDriver();
//                        driver = ps.getDriverOrder().get(i);
//                        if (currDriver.equals(driver)) {
//                            continue;
//                        }
//
//                        action = new Action(actionType, driver);
//                        validActions.add(action);
//                    }
//                    break;
//                case 4: // CHANGE TIRE TYPE
//                    for (int i = 0; i < ps.getTireOrder().size(); i++) {
//                        Tire currTire =
//                                currState.getTireModel();
//                        tire = ps.getTireOrder().get(i);
//
//                        if (currTire.equals(tire)) {
//                            continue;
//                        }
//
//                        action = new Action(actionType, tire);
//                        validActions.add(action);
//                    }
//                    break;
//                case 5: // ADD FUEL TO CAR
//                    break;
//                case 6: // ADD ALL VALID PRESSURES
//                    for (int i = 0; i < validPressures.size(); i++) {
//                        pressure = validPressures.get(i);
//                        action = new Action(actionType, pressure);
//                        validActions.add(action);
//                    }
//                    break;
//                case 7: // CHANGE CAR AND DRIVER
//                    for (int i = 0; i < ps.getDriverOrder().size(); i++) {
//                        String currDriver =
//                                currState.getDriver();
//                        driver = ps.getDriverOrder().get(i);
//                        if (currDriver.equals(driver)) {
//                            continue;
//                        }
//
//                        for (int j = 0; j < ps.getCarOrder().size(); j++) {
//                            String currCar = currState.getCarType();
//                            car = ps.getCarOrder().get(j);
//                            if (currCar.equals(car)) {
//                                continue;
//                            }
//
//                            action = new Action(actionType, car, driver);
//                            validActions.add(action);
//                        }
//                    }
//                    break;
//                default: // CHANGE TIRE FUEL AND PRESSURE
//                    for (int i = 0; i < ps.getTireOrder().size(); i++) {
//                        Tire currTire =
//                                currState.getTireModel();
//                        tire = ps.getTireOrder().get(i);
//                        if (currTire.equals(tire)) {
//                            continue;
//                        }
//
//                        for (int j = 0; j < validPressures.size(); j++) {
//                            pressure = validPressures.get(j);
//
//                            int currentFuel = currState.getFuel();
//                            int fuelToFill = 50 - currentFuel;
//                            fuel = (fuelToFill/10) * 10;
//
//                            action = new Action(actionType, tire, fuel, pressure);
//                            validActions.add(action);
//                        }
//                    }
//            }
//        }
//
//    }
//
//
//    public Action getBestAction (State state) {
//        Map<Action, Double> actionValues = new HashMap<>();
//
//        // This does the "value iteration => Q(s, a)" producing the values to
//        // be checked in the next for loop.
//        for (Action action : validActions) {
//            double bestValue = getBestValue(state, action);
//            actionValues.put(action, bestValue);
//        }
//
//        // This finds the largest value and sets the corressponding action to
//        // the local best Action until for loop completes
//        double max = 0;
//        Action bestAct = new Action(ActionType.MOVE);
//        for (Action actionKey : actionValues.keySet()) {
//            if (actionValues.get(actionKey) > max) {
//                max = actionValues.get(actionKey);
//                bestAct = actionKey;
//            }
//        }
//
//        return bestAct;
//
//    }
//
//
//    public double getBestValue(State currentState) {
//
//        // BASE CASE!:
//        if (currentState.getPos() >= ps.getN()) {
//            return MAX_REWARD;
//            // V*(s) = R(s) + DF*SUM[T(s, a, s')*V*(s')
//            // Where R(s) = 1 = MAX_REWARD;
//            // AND T(s,currState a, s') = 0
//        }
//
//        //RECURSIVE CASE:
//        double sum = 0;
//        for(Action nextAction : validActions) {
//            OurSim sim = new OurSim(ps, currentState);
//            // ADD CASES LIKE IN MCTS ------ FOR RUDY SINCE HES PLAYING
//            // GUITAR -------------------------------------------------
//            State nextState = sim.step(nextAction);
//            sum += getBestValue(nextState)*
//                    transitionFunction(currentState, nextAction, nextState);
//        }
//
//        double immediateReward = getForwardMoveProbs(currentState);
//        return immediateReward + discountFactor*sum;
//    }
//
//
//    public double transitionFunction(State currState, Action action,
//                                     State nextState) {
//
//        // ONLY MOVE ACTIONS HAVE A PROBABILITY
//        if (action.getActionType().getActionNo() == 1) {
//            int movement = nextState.getPos() - currState.getPos();
//
//            OurSim tempSim = new OurSim(ps, currState);
//            double[] probs = tempSim.getMoveProbs();
//
//            if (movement >= -4 && movement <= 5) {
//                int index = movement + 4;
//                return probs[index];
//            } else {
//                System.err.println("SOMETHING WRONG IN TRANSITION");
//                System.exit(6);
//                return -100;
//            }
//        } else {
//            return 1; // Assume deterministic for other actions
//        }
//    }
//
//
//    // ******************** RUDY ATTEMPT NUMBER 92323048234 *******************
//    public
//
//
//
//    /** Select a random action
//     *
//     * @param ps the problem spec
//     * @return a random valid action for problem spec
//     */
//    private Action selectRandomAction(ProblemSpec ps) {
//
//        int fuel;
//        String car, driver;
//        Tire tire;
//        TirePressure pressure;
//
//        List<ActionType> validActionTypes = ps.getLevel().getAvailableActions();
//        List<TirePressure> validPressures = new LinkedList<>();
//        validPressures.add(TirePressure.FIFTY_PERCENT);
//        validPressures.add(TirePressure.SEVENTY_FIVE_PERCENT);
//        validPressures.add(TirePressure.ONE_HUNDRED_PERCENT);
//
//        int numActions = validActionTypes.size();
//        int CT = ps.getCT();
//        int DT = ps.getDT();
//        int TiT = ProblemSpec.NUM_TYRE_MODELS;
//        int PressureT = ProblemSpec.TIRE_PRESSURE_LEVELS;
//
//        ActionType actionType = validActionTypes.get(aRandomInt(0, numActions));
//        Action action;
//
//        switch(actionType.getActionNo()) {
//            case 1:
//                action = new Action(actionType);
//                break;
//            case 2:
//                car = ps.getCarOrder().get(aRandomInt(0, CT));
//                action = new Action(actionType, car);
//                break;
//            case 3:
//                driver = ps.getDriverOrder().get(aRandomInt(0, DT));
//                action = new Action(actionType, driver);
//                break;
//            case 4:
//                tire = ps.getTireOrder().get(aRandomInt(0, TiT));
//                action = new Action(actionType, tire);
//                break;
//            case 5:
//                fuel = aRandomInt(ProblemSpec.FUEL_MIN, ProblemSpec.FUEL_MAX);
//                action = new Action(actionType, fuel);
//                break;
//            case 6:
//                pressure = validPressures.get(aRandomInt(0, PressureT));
//                action = new Action(actionType, pressure);
//                break;
//            case 7:
//                car = ps.getCarOrder().get(aRandomInt(0, CT));
//                driver = ps.getDriverOrder().get(aRandomInt(0, DT));
//                action = new Action(actionType, car, driver);
//                break;
//            default:
//                tire = ps.getTireOrder().get(aRandomInt(0, TiT));
//                fuel = aRandomInt(ProblemSpec.FUEL_MIN, ProblemSpec.FUEL_MAX);
//                pressure = validPressures.get(aRandomInt(0, PressureT));
//                action = new Action(actionType, tire, fuel, pressure);
//        }
//
//        return action;
//    }
//
//    /** I'm a helper method */
//    private static int aRandomInt(int min, int max) {
//
//        if (min >= max) {
//            throw new IllegalArgumentException("max must be greater than min");
//        }
//
//        Random r = new Random();
//        return r.nextInt((max - min)) + min;
//    }
//
//    public double getForwardMoveProbs(State trialState) {
//        OurSim tempSim = new OurSim(ps, trialState);
//        double[] probs = tempSim.getMoveProbs();
//
//        double sum = 0;
//        for (int i = 5; i<= 9; i++) {
//            sum += probs[i];
//        }
//
////        System.out.println("FORWARD MOVE PROB OF " + trialState + "= " + sum);
//        return sum;
//    }
//
//}
