package solution;

import problem.Action;
import problem.Terrain;
import simulator.State;

import java.util.ArrayList;
import java.util.List;

public class Node {

    /**
     * Set up global variables for a node type abstract data structure
     */
    private Node parent;
    private List<Node> children = new ArrayList<Node>();
    private State state;
    private float numSims = 0;
    private float numWins = 0;
    private double explore = Math.sqrt(2);
    private double weight = 0;
    private int stepsFromRoot;
    private Action parentAction;

    /**
     * Empty constructor for the Node class
     */
    public Node() {
    }

    /**
     * Constuctor for Node class.
     *
     * @param parent The parent node of this child.
     * @param state  The state within this node.
     */
    public Node(Node parent, State state, int steps, Action parentAction) {
        this.parent = parent;
        this.state = state;
        this.stepsFromRoot = steps;
        this.parentAction = parentAction;
    }

    // ******* LIST OF THE GETTER FUNCTIONS FOR THE PRIVATE MEMBERS **********

    public int getStepsFromRoot() {
        return stepsFromRoot;
    }

    public double getWeight() {
        updateWeight();
        return this.weight;
    }

    /**
     * returns deep copy of this nodes' state instance
     *
     * @return
     */
    public State getState() {
        return this.state.copyState();
    }

    /**
     * Return the size of the children
     */
    public int getChildrenSize() {
        return children.size();
    }

    public float getSims() {
        return this.numSims;
    }

    public float getWins() {
        return this.numWins;
    }

    public Action getParentAction() { return this.parentAction; }

    /**
     * Returns a deep copy of the children of this Node.
     *
     * @return
     */
    public List<Node> getChildren() {
//        List<Node> tempNodes = new ArrayList<>();
//        for (int i = 0; i <= getChildrenSize(); i++) {
//            Node node = new Node(this.copyNode(),
//                    children.get(i).getState(), this.stepsFromRoot, this.parentAction);
//            tempNodes.add(node);
//        }
//        return tempNodes;
        return children;
    }



    // ******* LIST OF THE SETTER FUNCTIONS FOR THE PRIVATE MEMBERS **********

    public void addWins(float newWins) {
        this.numWins += newWins;
        if (this.parent!=null) {
            this.parent.addWins(newWins);
        }
    }

    public void addSims(float newSims) {
        this.numSims += newSims;
        if (this.parent!=null) {
            this.parent.addSims(newSims);
        }
    }

    public void addWin(){
        this.numWins += 1;
        if (this.parent!=null) {
            this.parent.addWin();
        }
    }

    public void addSim(){
        this.numSims += 1;
        if (this.parent!=null) {
            this.parent.addSim();
        }
    }

    private void updateWeight() {
        if (this.parent != null) {
            // sp: This nodes parents' total number of simulations;
            float sp = parent.getSims();
            weight =
                    (numWins / numSims) + explore * (Math.sqrt((Math.log(sp)) / (numSims)));
        }
    }

    /**
     * updates the children instance with a new Child node.
     *
     * @param newChild
     */
    public void updateChildren(Node newChild) {
        this.children.add(newChild);
    }

    public void resetParent() {
        this.parent = null;
        this.parentAction = null;
    }


    // ************ OTHER FUNCTIONS REQUIRED **********


    /**
     * Returns a deep copy of a Node
     *
     * @return
     */
    public Node copyNode() {
        Node tempNode;
        if (parent == null || parentAction == null) {
            tempNode = new Node(null, this.state.copyState(), 0,
                    null);
        } else {
            tempNode = new Node(parent.copyNode(), this.state.copyState(),
                    this.stepsFromRoot, this.parentAction);
        }

        if (getChildrenSize() > 0) {
            List<Node> childrenNodes = getChildren();
            for (int i = 0; i < getChildrenSize(); i++) {
                tempNode.updateChildren(childrenNodes.get(i));
            }
        }

        tempNode.addWins(this.numWins);
        tempNode.addSims(this.numSims);

        return tempNode;
    }


//    public void backPropogate(float numWins, float numSims) {
//
//        this.parent.addWins(numWins);
//        this.parent.addSims(numSims);
//
//        if (this.parent != null) {
//            this.parent.backPropogate(numWins, numSims);
//        }
//    }
}
