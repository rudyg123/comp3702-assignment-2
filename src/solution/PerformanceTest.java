package solution;

import java.util.ArrayList;
import java.util.List;

public class PerformanceTest {

    private Runtime runtime;
//    private static final long MEGABYTE = 1024L * 1024L;

    private List<Long> mems = new ArrayList<>();
    private List<Long> times = new ArrayList<>();;
    private List<Integer> numExpandedNodes = new ArrayList<>();
    private List<Integer> stepNumbers = new ArrayList<>();
    private long startTime;


    public PerformanceTest() {
        this.runtime = Runtime.getRuntime();
        this.startTime = System.currentTimeMillis();
    }

    public void step() {
        getTime();
        getMemory();
    }

    private void getTime() {
        long time = System.currentTimeMillis();
        long diff = time - startTime;
        if (diff > 9) {
            this.times.add(diff);
        } else {
            this.times.add((long)0);
        }
        this.startTime = time;
    }

    private void getMemory() {
        // Run the garbage collector
        this.runtime.gc();

        long memory = runtime.totalMemory() - runtime.freeMemory(); // In bytes
        this.mems.add(memory);
    }

    public void getNumExpandedNodes(int num) {
        this.numExpandedNodes.add(num);
    }

    public void getStepNumbers(int step) {
        this.stepNumbers.add(step);
    }

//    public static long bytesToMegabytes(long bytes) {
//        return bytes / MEGABYTE;
//    }

//    public List<Long> getTimes() {
//        return this.times;
//    }
//
//    public List<Long> getMem() {
//        return this.mems;
//    }

    public String toString() {
        String str = "Times: (ms)\n";

        for (int i = 0; i < times.size(); i++) {
            str = str + times.get(i) + "\n";
        }

        str = str + "\n Memory (Bytes)\n";

        for (int i = 0; i < mems.size(); i++) {
            str = str + mems.get(i) + "\n";
        }

        str = str + "\nExpanded Nodes\n";

        for (int i : numExpandedNodes) {
            str = str + i + "\n";
        }

        str = str + "\nNumber Steps\n";

        for (int j : stepNumbers) {
            str = str + j + "\n";
        }

        str = str + "\n";

        return str;
    }
}
