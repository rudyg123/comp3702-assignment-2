README

BEFORE COMPILE
* search for 'edit the system envioronment variables' in search bar
* select 'Envioronment Variables' towards bottom of popup
* Under 'User variables for 'user' select Path and then Edit...
* Select new, then input path to java.exe, this should be in C:\Program Files\Java\jdk-11.0.1\bin
* Under 'System variables' select New...
* In variable name put JAVA_HOME and in variable value put the same path as above C:\Program Files\Java\jdk-11.0.1\bin
* restart cmd if open

HOW TO COMPILE
CMD:
* cd 'path of src folder'
	-> e.g. cd C:\Users\jdhar\Documents\COMP3702_ASS_2\src
* javac problem/*.java simulator/*.java solution/*.java
* java problem/Main 'input file name' 'output file name'

Note:
input file needs to be in src folder to be able to be read
