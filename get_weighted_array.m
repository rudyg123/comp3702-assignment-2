function array = get_weighted_array(totalProbMovingForward)

    ProbMovingForward = totalProbMovingForward/5;

    totalProbMovingBack = 1-totalProbMovingForward;
    ProbMovingBack = totalProbMovingBack/7;

    array = ones(1, 12)*ProbMovingBack;
    array(6:10) = ProbMovingForward;
end